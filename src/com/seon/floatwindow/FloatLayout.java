package com.seon.floatwindow;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FloatLayout extends LinearLayout {
	private float actionDownX; // event down x
	private float actionDownY; // event down y
	private float downX; // LayoutParams x
	private float downY; // LayoutParams y

	private float moveX; // event move x
	private float moveY; // event move y
	private float upX;// 触发MotionEvent.ACTION_UP时的x坐标，mLayoutParams中的x的值
	private float upY;// 触发MotionEvent.ACTION_UP时的y坐标，mLayoutParams中的y的值

	private WindowManager.LayoutParams mLayoutParams;
	private WindowManager mWinManager;

	private boolean isLocked = false;

	// 用户触摸悬浮图标时有没有触发MotionEvent.ACTION_MOVE，如果触发了，我们将不进行点击事件的处理
	private boolean isMoveAction = false;

	// 根据两次点击的时间间隔判断是双击还是单击操作
//	private boolean isDoubleClick = false;

	private long firstTime = 0l;// 用户第一次触摸悬浮图标时的时间
	private long secondTime = 0l;// 用户第二次触摸悬浮图标时的时间

	private TextView downSpeed;
	private TextView upSpeed;

	public FloatLayout(Context context, WindowManager.LayoutParams params) {
		super(context);
		mWinManager = (WindowManager) getContext().getApplicationContext()
				.getSystemService(Service.WINDOW_SERVICE);
		mLayoutParams = params;
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(R.layout.float_window, this);
	}

	int startX, startY, fixY,fixX;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			// 按下事件的起始位置
			downX = event.getRawX();
			downY = event.getRawY();

			startX = mLayoutParams.x;
			startY = mLayoutParams.y;

			// 按下事件的相对mLayout的位置
			actionDownX = event.getX();
			actionDownY = event.getY();
			fixX = (int) ((downX - actionDownX) - (startX > 0 ? startX : 0));
			fixY = (int) ((downY - actionDownY) - (startY > 0 ? startY : 0));
			break;
		case MotionEvent.ACTION_MOVE:
			moveX = event.getRawX();
			moveY = event.getRawY();
			updateViewPosition(false);
			isMoveAction = true;
			break;
		case MotionEvent.ACTION_UP:
			upX = mLayoutParams.x;
			upY = mLayoutParams.y;
			updateViewPosition(true);

			firstTime = System.currentTimeMillis();
			if (!isMoveAction) {
				if (firstTime - secondTime < 600) {
//					isDoubleClick = true;
					isLocked = !isLocked;
					this.setEnabled(isLocked);
				}
			}
			secondTime = firstTime;// 记录上一次触摸的时间
			isMoveAction = false;
			break;
		}

		return true;
	}

	private void updateViewPosition(boolean isActionUp) {
		if (!isLocked) {
			if (!isActionUp) {
				mLayoutParams.x = (int) (moveX - actionDownX);
				mLayoutParams.y = (int) (moveY - actionDownY) - fixY;
			} else {
				if (Math.abs(upX - startX) < 20 & Math.abs(upY - startY) < 20) {
					isMoveAction = false;
					mLayoutParams.x = startX;
					mLayoutParams.y = startY;
				}
			}
			mWinManager.updateViewLayout(this, mLayoutParams);
		}
	}

	public void updateInfo(long up, long down) {
		upSpeed = (TextView) findViewById(R.id.upstream);
		downSpeed = (TextView) findViewById(R.id.downstream);
		upSpeed.setText(convertStorage(up));
		downSpeed.setText(convertStorage(down));
	}

	public void openActivity() {
		Intent intent = new Intent(getContext(), MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getContext().startActivity(intent);
	}

	public static String convertStorage(long size) {
		long kb = 1024;
		long mb = kb * 1024;
		long gb = mb * 1024;

		if (size >= gb) {
			return String.format("%.1fG", (float) size / gb);
		} else if (size >= mb) {
			float f = (float) size / mb;
			return String.format(f > 100 ? "%.0fM" : "%.1fM", f);
		} else if (size >= kb) {
			float f = (float) size / kb;
			return String.format(f > 100 ? "%.0fK" : "%.1fK", f);
		} else{
			return String.format("%d", size);
		}
	}

}
