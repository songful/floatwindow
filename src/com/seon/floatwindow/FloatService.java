package com.seon.floatwindow;

import java.text.DecimalFormat;
import java.text.Format;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.PixelFormat;
import android.net.TrafficStats;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FloatService extends Service{

	private final static int FLOAT_WIDTH = 200;
	private final static int FLOAT_HEIGHT = 100;
	private final static int DELAY_TIME = 1000;
	private final static int START_X = 100;
	private final static int START_Y = 100;
	
	public static final int NOTIFICATION_ID = 1988;
	private final String PREF_NAME = "pref";
	private SharedPreferences mPref;// 用于保存关闭悬浮窗口时悬浮窗口的坐标
	private Editor mEditor;
	
	private TextView upView;
	private TextView downView;
	
	LinearLayout mLayout;
	WindowManager.LayoutParams mLayoutParams;
	WindowManager mWinManager;
	
	@Override
	public void onCreate() {
		super.onCreate();
		createWindow();
		mLayoutonTouch();
		task.start();
	}

	private void createWindow() {
		mPref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		LayoutInflater inflater = LayoutInflater.from(getApplication());
		mLayout =  (LinearLayout) inflater.inflate(R.layout.float_window, null);
		
		mLayoutParams = new WindowManager.LayoutParams();
		mWinManager = (WindowManager) getApplication().getSystemService(WINDOW_SERVICE);
		
		//设置悬浮框x、y初始值
		mLayoutParams.x = mPref.getInt("x", START_X);
		mLayoutParams.y = mPref.getInt("y", START_Y);
		//设置悬浮框长宽
		mLayoutParams.height = FLOAT_HEIGHT;
		mLayoutParams.width = FLOAT_WIDTH;
		mLayoutParams.gravity = Gravity.LEFT | Gravity.TOP; // 以屏幕左上角为原点
		
		mLayoutParams.flags = LayoutParams.FLAG_NOT_TOUCH_MODAL 
					| LayoutParams.FLAG_NOT_FOCUSABLE;// 允许window以外的区域接受点击,不能获得焦点
		mLayoutParams.type = LayoutParams.TYPE_PHONE;	//悬浮框置顶
		mLayoutParams.format = PixelFormat.RGBA_8888;
		mWinManager.addView(mLayout, mLayoutParams);
	}

	private void mLayoutonTouch() {
		mLayout.setOnTouchListener(new View.OnTouchListener() {
			int lastX, lastY;
        	int paramX, paramY;
			@Override
			public boolean onTouch(View v, MotionEvent event) {
					switch(event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						lastX = (int) event.getRawX();
						lastY = (int) event.getRawY();
						paramX = mLayoutParams.x;
						paramY = mLayoutParams.y;
						break;
					case MotionEvent.ACTION_MOVE:
						int dx = (int) event.getRawX() - lastX;
						int dy = (int) event.getRawY() - lastY;
						mLayoutParams.x = paramX + dx;
						mLayoutParams.y = paramY + dy;
						// 更新悬浮窗位置
				        mWinManager.updateViewLayout(mLayout, mLayoutParams);
						break;
					}
					return true;
				
			}
		});
	}
	
	Handler handler = new Handler(){
		double up, down ;
		DecimalFormat df1 = new DecimalFormat("##0.00 ");
		DecimalFormat df2 = new DecimalFormat("##0.00 K");
		@Override
		public void handleMessage(Message msg){

			up = msg.getData().getDouble("up");
			down = msg.getData().getDouble("down");
			if(up < 600){
				upView.setText(df1.format(up/1024));
			}else{
				upView.setText(df2.format(up/1024));
			}
			if(down < 600){
				downView.setText(df1.format(down/1024));
			}else{
				downView.setText(df2.format(down/1024));
			}
			
		}
	};
	
	Thread task =  new Thread(){

		@Override
		public void run(){
			while(true){
				long d1 = TrafficStats.getTotalRxBytes();
				long u1 = TrafficStats.getTotalTxBytes();
				try {
					Thread.sleep(DELAY_TIME);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				double d = (TrafficStats.getTotalRxBytes() - d1);
				double u = (TrafficStats.getTotalTxBytes() - u1);
				
				Message msg = handler.obtainMessage(1, "流量显示");
				Bundle data = new Bundle();
				data.putDouble("up", u);
				data.putDouble("down", d);
				msg.setData(data);
				handler.sendMessage(msg);
			}
		}
	};
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {	
		Notification n = new Notification();
		startForeground(NOTIFICATION_ID, n);
		upView = (TextView) mLayout.findViewById(R.id.upstream);
		downView = (TextView) mLayout.findViewById(R.id.downstream);
		
		return Service.START_REDELIVER_INTENT;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		// 在service结束时保存悬浮窗口的位置
		mEditor = mPref.edit();
		mEditor.putInt("x", mLayoutParams.x);
		mEditor.putInt("y", mLayoutParams.y);
		mEditor.commit();
		
		mWinManager.removeView(mLayout);
		
		// service结束时销毁Notification
		NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(NOTIFICATION_ID);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	

}
