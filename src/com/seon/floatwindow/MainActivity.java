package com.seon.floatwindow;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

public class MainActivity extends Activity {
	private final String PREF_NAME = "pref";
	private SharedPreferences mPref;// 用于保存关闭悬浮窗口时悬浮窗口的坐标
	private Editor mEditor;
	private Switch kit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

	@Override
	protected void onStop() {
		super.onStop();
    	mEditor = mPref.edit();
    	mEditor.putBoolean("switch", kit.isChecked());
    	mEditor.commit();
	}



	@Override
	protected void onStart() {
		super.onStart();
		kit = (Switch) findViewById(R.id.switch1);
    	kit.setChecked(mPref.getBoolean("switch", false));
	}



	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			final Switch kit = (Switch) getActivity().findViewById(R.id.switch1);

			kit.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(kit.isChecked()){
						Intent intent = new Intent(getActivity(), WinService.class);
						getActivity().startService(intent);
						getActivity().finish();
					}else{
						Intent intent = new Intent(getActivity(), WinService.class);
						getActivity().stopService(intent);
					}
				}
			});
		}
		
         
    }

}
