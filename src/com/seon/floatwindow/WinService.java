package com.seon.floatwindow;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.PixelFormat;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;

public class WinService extends Service {
	private final static int FLOAT_WIDTH = 150;
	private final static int FLOAT_HEIGHT = 60;
	private final static int DELAY_TIME = 1000;
	private final static int START_X = 0;
	private final static int START_Y = 0;
	private final static int MSG_WHAT_UPDATE = 1;
	public static final int NOTIFICATION_ID = 1988;
	private final String PREF_NAME = "pref";
	private SharedPreferences mPref;// 用于保存关闭悬浮窗口时悬浮窗口的坐标
	private Editor mEditor;
	
	WindowManager.LayoutParams mLayoutParams;
	WindowManager mWinManager;
	FloatLayout mLayout;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mPref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		cresteView();
		mTask.start();
		return Service.START_REDELIVER_INTENT;
	}
	
	private Thread mTask = new Thread(){
		@Override
		public void run(){
			while(true){
				long d1 = TrafficStats.getTotalRxBytes();
				long u1 = TrafficStats.getTotalTxBytes();
				try {
					Thread.sleep(DELAY_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				long d = (TrafficStats.getTotalRxBytes() - d1);
				long u = (TrafficStats.getTotalTxBytes() - u1);
				
				Message msg = mHandler.obtainMessage(MSG_WHAT_UPDATE, "流量显示");
				Bundle data = new Bundle();
				data.putLong("up", u);
				data.putLong("down", d);
				msg.setData(data);
				mHandler.sendMessage(msg);
			}
		}	
	};
	
	private Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
			case MSG_WHAT_UPDATE: 
				long up = msg.getData().getLong("up");
				long down = msg.getData().getLong("down");
				mLayout.updateInfo(up,down);
			}
		}
	};

	private void cresteView() {
		if(mLayout == null){
			
			mLayoutParams = new WindowManager.LayoutParams();
			mWinManager = (WindowManager) getApplication().getSystemService(WINDOW_SERVICE);
			
			//设置悬浮框x、y初始值
			mLayoutParams.x = mPref.getInt("x", START_X);
			mLayoutParams.y = mPref.getInt("y", START_Y);
			//设置悬浮框长宽
			mLayoutParams.height = FLOAT_HEIGHT;
			mLayoutParams.width = FLOAT_WIDTH;
			mLayoutParams.gravity = Gravity.LEFT | Gravity.TOP; // 以屏幕左上角为原点
			
			mLayoutParams.flags = LayoutParams.FLAG_NOT_TOUCH_MODAL 
						| LayoutParams.FLAG_NOT_FOCUSABLE;// 允许window以外的区域接受点击,不能获得焦点
			mLayoutParams.type = LayoutParams.TYPE_PHONE;	//悬浮框置顶
			mLayoutParams.format = PixelFormat.RGBA_8888;
			
			mLayout = new FloatLayout(getApplicationContext(), mLayoutParams);	
			mWinManager.addView(mLayout, mLayoutParams);
			
			Notification n = new Notification();
			startForeground(NOTIFICATION_ID, n);
			
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// 在service结束时保存悬浮窗口的位置
		mEditor = mPref.edit();
		mEditor.putInt("x", mLayoutParams.x);
		mEditor.putInt("y", mLayoutParams.y);
		mEditor.commit();
				
		mWinManager.removeView(mLayout);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
